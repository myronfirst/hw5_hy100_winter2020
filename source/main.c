#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "Algorithm.h"
#include "List.h"
#include "Print.h"
#include "XPM.h"

#include "Globals.h"

void GetInput() {
    int ID = 0;
    int ret;
    FILE* file = fopen(FileName, "r");
    ret = fscanf(file, "%d %d %d\n", &TotalRects, &MaxWidth, &MaxHeight);
    assert(ret > 0);
    assert(TotalRects > 0);
    int x, y, w, h, filled;
    char color[8];
    ret = fscanf(file, "%d %d %d %d %s %d\n", &x, &y, &w, &h, color, &filled);
    assert(ret > 0);
    Head = CreateNode(ID, x, y, w, h, color, filled);
    ID++;
    Node* prev = Head;
    for (int i = 1; i < TotalRects; ++i) {
        ret = fscanf(file, "%d %d %d %d %s %d\n", &x, &y, &w, &h, color, &filled);
        assert(ret > 0);
        Node* node = CreateNode(ID, x, y, w, h, color, filled);
        ID++;
        prev->next = node;
        prev = node;
    }
    fclose(file);
}

void GetInputSorted() {
    int ID = 0;
    int ret;
    FILE* file = fopen(FileName, "r");
    ret = fscanf(file, "%d %d %d\n", &TotalRects, &MaxWidth, &MaxHeight);
    assert(ret > 0);
    assert(TotalRects > 0);
    int x, y, w, h, filled;
    char color[8];
    ret = fscanf(file, "%d %d %d %d %s %d\n", &x, &y, &w, &h, color, &filled);
    assert(ret > 0);
    Node* node = CreateNode(ID, x, y, w, h, color, filled);
    ID++;
    InsertSorted(&HeadSorted, node);
    for (int i = 1; i < TotalRects; ++i) {
        ret = fscanf(file, "%d %d %d %d %s %d\n", &x, &y, &w, &h, color, &filled);
        assert(ret > 0);
        node = CreateNode(ID, x, y, w, h, color, filled);
        ID++;
        InsertSorted(&HeadSorted, node);
    }
    fclose(file);
}

void Input(const char* fileName) {
    FileName = fileName;
    GetInput();
    GetInputSorted();
}

void Algorithms() {
    MinAreaNode = FindMinAreaNode(Head);
    MaxAreaNode = FindMaxAreaNode(Head);
    AreaSortedHead = SortByArea(Head);
    ColorSortedHead = SortByColor(Head);
    MinAreaNodeSorted = GetHead(HeadSorted);
    MaxAreaNodeSorted = GetTail(HeadSorted);
}

void PrintOutput() {
    // PrintMinMax();
    // printf("\n");
    // PrintRectsByArea();
    // printf("\n");
    // PrintRectsByColor();

    PrintMinMaxSorted();
    printf("\n");
    PrintRectsByAreaSorted();
    printf("\n");
    PrintRectsByColorSorted();
}

void FreeLists() {
    DestroyList(Head);
    DestroyList(HeadSorted);
    DestroyList(AreaSortedHead);
    DestroyList(ColorSortedHead);
}

void XPM() {
    char** canvas = CreateCanvas();
    FillCanvas(canvas, Head);
    char xpmFileName[1024] = { 0 };
    GetXPMFileName(xpmFileName, FileName);
    WriteXPM(xpmFileName, canvas);
    DestroyCanvas(canvas);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("Invalid input\n");
        exit(EXIT_FAILURE);
    }
    Input(argv[1]);
    Algorithms();
    PrintOutput();
    XPM();
    FreeLists();
    return 0;
}
