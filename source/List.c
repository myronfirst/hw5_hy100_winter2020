#include "List.h"

#include "Globals.h"

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Color GetColor(const char* color) {
    Color c;
    if (strcmp(color, "red") == 0)
        c = RED;
    else if (strcmp(color, "green") == 0)
        c = GREEN;
    else if (strcmp(color, "blue") == 0)
        c = BLUE;
    else if (strcmp(color, "yellow") == 0)
        c = YELLOW;
    else if (strcmp(color, "cyan") == 0)
        c = CYAN;
    else if (strcmp(color, "magenta") == 0)
        c = MAGENTA;
    else if (strcmp(color, "black") == 0)
        c = BLACK;
    else if (strcmp(color, "white") == 0)
        c = WHITE;
    else
        assert(0);
    return c;
}

const char* GetColorString(Color color) {
    switch (color) {
        case RED: return "red"; break;
        case GREEN: return "green"; break;
        case BLUE: return "blue"; break;
        case YELLOW: return "yellow"; break;
        case CYAN: return "cyan"; break;
        case MAGENTA: return "magenta"; break;
        case BLACK: return "black"; break;
        case WHITE: return "white"; break;
        default: assert(0); break;
    }
}

int GetArea(Node* n) {
    int area = n->data.w * n->data.h;
    assert(area > 0);
    return area;
}

int AreaIsLess(Node* a, Node* b) {
    int areaA = GetArea(a);
    int areaB = GetArea(b);
    int diff = areaA - areaB;
    if (diff == 0) diff = a->data.id - b->data.id;
    return diff < 0;
}

void CheckError(int id, int x, int y, int w, int h) {
    if (((x >= 0) && (y >= 0) && ((x + w) <= MaxWidth) && ((y + h) <= MaxHeight)) == 0) {
        printf("rectangle %d has wrong definition\n", id);
        exit(EXIT_FAILURE);
    }
}

Node* CreateNode(int id, int x, int y, int w, int h, const char* color, int filled) {
    CheckError(id, x, y, w, h);
    Node* n = (Node*)calloc(1, sizeof(Node));
    n->data.id = id;
    n->data.x = x;
    n->data.y = y;
    n->data.w = w;
    n->data.h = h;
    n->data.color = GetColor(color);
    n->data.filled = filled;
    n->next = NULL;
    return n;
}

void DestroyNode(Node* n) {
    memset(n, 0, sizeof(Node));
    free(n);
}

void DestroyList(Node* head) {
    Node* prev = NULL;
    for (Node* n = head; n != NULL; n = n->next) {
        if (prev != NULL) DestroyNode(prev);
        prev = n;
    }
    DestroyNode(prev);
}

void InsertSorted(Node** head, Node* n) {
    assert(head);
    Node* prev = NULL;
    Node* node = *head;
    for (node = *head; (node != NULL) && (AreaIsLess(node, n)); node = node->next)
        prev = node;

    n->next = node;
    if (prev == NULL)
        *head = n;
    else
        prev->next = n;
}

void CopyNodeTo(Node* dest, Node* src) {
    memcpy(dest, src, sizeof(Node));
    dest->next = NULL;
}

Node* CopyNode(Node* src) {
    Node* dest = calloc(1, sizeof(Node));
    CopyNodeTo(dest, src);
    return dest;
}

Node* ToArray(Node* head) {
    assert(head);
    Node* array = calloc(TotalRects, sizeof(Node));
    int i = 0;
    for (Node* node = head; node != NULL; node = node->next) {
        CopyNodeTo(&(array[i]), node);
        i++;
    }
    return array;
}
Node* ToList(Node* array) {
    assert(array);
    Node* head = CopyNode(&(array[0]));
    Node* prev = head;
    for (int i = 1; i < TotalRects; ++i) {
        Node* node = CopyNode(&(array[i]));
        prev->next = node;
        prev = node;
    }
    return head;
}
