#include "Print.h"

#include "Globals.h"
#include "List.h"

#include <stdio.h>
#include <stdlib.h>

void PrintMinMax() {
    printf("min area: %d: %d\n", MinAreaNode->data.id, GetArea(MinAreaNode));
    printf("max area: %d: %d\n", MaxAreaNode->data.id, GetArea(MaxAreaNode));
}
void PrintRectsByArea() {
    printf("rectangles by area:\n");
    for (Node* node = AreaSortedHead; node != NULL; node = node->next) {
        printf("%d: %d\n", node->data.id, GetArea(node));
    }
}
void PrintRectsByColor() {
    printf("rectangles by color:\n");
    for (Color color = RED; color < WHITE; ++color) {
        int firstId = 1;
        printf("%s:", GetColorString(color));
        for (Node* node = ColorSortedHead; node != NULL; node = node->next) {
            if (color != node->data.color) continue;
            if (firstId) {
                printf(" %d", node->data.id);
                firstId = 0;
            } else {
                printf(", %d", node->data.id);
            }
        }
        printf("\n");
    }
}

void PrintMinMaxSorted() {
    printf("min area: %d: %d\n", MinAreaNode->data.id, GetArea(MinAreaNodeSorted));
    printf("max area: %d: %d\n", MaxAreaNode->data.id, GetArea(MaxAreaNodeSorted));
}
void PrintRectsByAreaSorted() {
    printf("rectangles by area:\n");
    for (Node* node = HeadSorted; node != NULL; node = node->next) {
        printf("%d: %d\n", node->data.id, GetArea(node));
    }
}
void PrintRectsByColorSorted() {
    printf("rectangles by color:\n");
    for (Color color = RED; color < WHITE; ++color) {
        int firstId = 1;
        printf("%s:", GetColorString(color));
        for (Node* node = HeadSorted; node != NULL; node = node->next) {
            if (color != node->data.color) continue;
            if (firstId) {
                printf(" %d", node->data.id);
                firstId = 0;
            } else {
                printf(", %d", node->data.id);
            }
        }
        printf("\n");
    }
}
