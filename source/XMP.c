#include "XPM.h"

#include "Globals.h"
#include "List.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

const char* GetHex(Color color) {
    switch (color) {
        case RED: return "#ff0000"; break;
        case GREEN: return "#00ff00"; break;
        case BLUE: return "#0000ff"; break;
        case YELLOW: return "#ffff00"; break;
        case CYAN: return "#00ffff"; break;
        case MAGENTA: return "#ff00ff"; break;
        case BLACK: return "#000000"; break;
        case WHITE: return "#ffffff"; break;
        default: assert(0); break;
    }
}
char GetSymbol(Color color) {
    switch (color) {
        case RED: return 'r'; break;
        case GREEN: return 'g'; break;
        case BLUE: return 'b'; break;
        case YELLOW: return 'y'; break;
        case CYAN: return 'c'; break;
        case MAGENTA: return 'm'; break;
        case BLACK: return '.'; break;
        case WHITE: return ' '; break;
        default: assert(0); break;
    }
}
void GetXPMFileName(char* xpmFileName, const char* name) {
    memset(xpmFileName, 0, 1024 * sizeof(char));
    strcpy(xpmFileName, name);
    char* p = strstr(xpmFileName, ".in");
    if (p == NULL) p = strstr(xpmFileName, ".txt");
    assert(p);
    int len = strlen(p);
    memset(p, 0, len);
    strcpy(p, ".xpm");
}
char** CreateCanvas() {
    char** canvas = calloc(MaxWidth, sizeof(char*));
    for (int x = 0; x < MaxWidth; ++x) {
        canvas[x] = calloc(MaxHeight, sizeof(char));
        for (int y = 0; y < MaxHeight; ++y) {
            canvas[x][y] = GetSymbol(WHITE);
        }
    }
    return canvas;
}
void FillCanvas(char** canvas, Node* head) {
    for (Node* node = head; node != NULL; node = node->next) {
        int startX = node->data.x;
        int endX = node->data.x + node->data.w;
        int startY = node->data.y;
        int endY = node->data.y + node->data.h;
        for (int x = startX; x < endX; ++x) {
            for (int y = startY; y < endY; ++y) {
                if (node->data.filled) {
                    canvas[x][y] = GetSymbol(node->data.color);
                } else {
                    if ((x == startX) || (x == (endX - 1)) || (y == startY) || (y == (endY - 1)))
                        canvas[x][y] = GetSymbol(node->data.color);
                }
            }
        }
    }
}
void DestroyCanvas(char** canvas) {
    for (int x = 0; x < MaxWidth; ++x) {
        free(canvas[x]);
        canvas[x] = NULL;
    }
    free(canvas);
}
void WriteXPM(const char* xpmFileName, char** canvas) {
    FILE* xpm = fopen(xpmFileName, "w");
    fprintf(xpm,
            "/* XPM */\n"
            "static char * canvas[] = {\n"
            "/* <Values> */\n"
            "/* <width/columns> <height/rows> <colors> <chars per pixel>*/\n");
    fprintf(xpm, "\"%d %d %d %d\",\n", MaxWidth, MaxHeight, COLOR_MAX, 1);
    fprintf(xpm, "/* <Colors> */\n");
    for (Color color = RED; color < COLOR_MAX; ++color)
        fprintf(xpm, "\"%c %c %s\",\n", GetSymbol(color), 'c', GetHex(color));
    fprintf(xpm, "/* <Pixels> */\n");
    for (int y = 0; y < MaxHeight; ++y) {
        fprintf(xpm, "\"");
        for (int x = 0; x < MaxWidth; ++x) {
            fprintf(xpm, "%c", canvas[x][y]);
        }
        if (y == MaxHeight - 1)
            fprintf(xpm, "\"\n");
        else
            fprintf(xpm, "\",\n");
    }
    fprintf(xpm, "};\n");
    fclose(xpm);
}
