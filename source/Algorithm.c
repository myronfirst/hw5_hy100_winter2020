#include "Algorithm.h"

#include "Globals.h"
#include "List.h"

#include <assert.h>
#include <limits.h>
#include <stdlib.h>

int AreaComparator(const void* _a, const void* _b) {
    Node* a = (Node*)_a;
    Node* b = (Node*)_b;
    int areaA = GetArea(a);
    int areaB = GetArea(b);
    int diff = areaA - areaB;
    if (diff == 0) diff = a->data.id - b->data.id;
    return diff;
}

int ColorComparator(const void* _a, const void* _b) {
    Node* a = (Node*)_a;
    Node* b = (Node*)_b;
    int colorA = (int)a->data.color;
    int colorB = (int)b->data.color;
    int diff = colorA - colorB;
    if (diff == 0) diff = a->data.id - b->data.id;
    return diff;
}

Node* FindMinAreaNode(Node* head) {
    int minArea = INT_MAX;
    Node* minAreaNode = head;
    for (Node* node = head; node != NULL; node = node->next) {
        int area = GetArea(node);
        if (area < minArea) {
            minAreaNode = node;
            minArea = area;
        } else if (area == minArea && node->data.id < minAreaNode->data.id) {
            minAreaNode = node;
            minArea = area;
        }
    }
    return minAreaNode;
}
Node* FindMaxAreaNode(Node* head) {
    int maxArea = INT_MIN;
    Node* maxAreaNode = head;
    for (Node* node = head; node != NULL; node = node->next) {
        int area = GetArea(node);
        if (area > maxArea) {
            maxAreaNode = node;
            maxArea = area;
        } else if (area == maxArea && node->data.id < maxAreaNode->data.id) {
            maxAreaNode = node;
            maxArea = area;
        }
    }
    return maxAreaNode;
}
Node* SortByArea(Node* head) {
    Node* array = ToArray(head);
    qsort(array, TotalRects, sizeof(Node), AreaComparator);
    Node* sortedHead = ToList(array);
    free(array);
    return sortedHead;
}
Node* SortByColor(Node* head) {
    Node* array = ToArray(head);
    qsort(array, TotalRects, sizeof(Node), ColorComparator);
    Node* sortedHead = ToList(array);
    free(array);
    return sortedHead;
}

Node* GetHead(Node* head) {
    assert(head);
    return head;
}

Node* GetTail(Node* head) {
    assert(head);
    Node* prev;
    for (Node* node = head; node != NULL; node = node->next)
        prev = node;

    return prev;
}
