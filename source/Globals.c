#include "Globals.h"

#include <stddef.h>

const char* FileName = NULL;
int TotalRects = -1;
int MaxWidth = -1;
int MaxHeight = -1;
struct Node* Head = NULL;

struct Node* MinAreaNode = NULL;
struct Node* MaxAreaNode = NULL;
struct Node* AreaSortedHead = NULL;
struct Node* ColorSortedHead = NULL;

struct Node* HeadSorted = NULL;
struct Node* MinAreaNodeSorted = NULL;
struct Node* MaxAreaNodeSorted = NULL;
