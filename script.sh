#!/bin/bash

TESTS=$(ls test | grep ".in")
#echo $TESTS

make clean
make
for entry in $TESTS
do
    path="test/$entry"
#    echo $path
#    ./hw5 $path
    valgrind --leak-check=full ./hw5 $path
done
