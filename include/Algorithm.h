#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

#include "List.h"
int AreaComparator(const void* _a, const void* _b);
int ColorComparator(const void* _a, const void* _b);
Node* FindMinAreaNode(Node* head);
Node* FindMaxAreaNode(Node* head);
Node* SortByArea(Node* head);
Node* SortByColor(Node* head);
Node* GetHead(Node* head);
Node* GetTail(Node* head);

#endif
