#ifndef _LIST_H_
#define _LIST_H_

enum Color {
    RED = 0,
    GREEN,
    BLUE,
    YELLOW,
    CYAN,
    MAGENTA,
    BLACK,
    WHITE,
    COLOR_MAX
};
typedef enum Color Color;

struct Rect {
    int id;
    int x, y, w, h;
    Color color;
    int filled;
};
typedef struct Rect Rect;

struct Node {
    Rect data;
    struct Node* next;
};
typedef struct Node Node;

Color GetColor(const char* color);
const char* GetColorString(Color color);
int GetArea(Node* n);
int AreaIsLess(Node* a, Node* b);
void CheckError(int id, int x, int y, int w, int h);
Node* CreateNode(int id, int x, int y, int w, int h, const char* color, int filled);
void InsertSorted(Node** head, Node* n);
void DestroyNode(Node* n);
void DestroyList(Node* head);
void CopyNodeTo(Node* dest, Node* src);
Node* CopyNode(Node* src);
Node* ToArray(Node* head);
Node* ToList(Node* array);

#endif
