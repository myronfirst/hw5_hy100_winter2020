#ifndef _PRINT_H_
#define _PRINT_H_

void PrintMinMax();
void PrintRectsByArea();
void PrintRectsByColor();

void PrintMinMaxSorted();
void PrintRectsByAreaSorted();
void PrintRectsByColorSorted();

#endif
