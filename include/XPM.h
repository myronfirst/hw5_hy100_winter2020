#ifndef _XPM_H_
#define _XPM_H_

#include "List.h"

#include <stdio.h>

const char* GetHex(Color color);
char GetSymbol(Color color);
void GetXPMFileName(char* xpmFileName, const char* name);
char** CreateCanvas();
void FillCanvas(char** canvas, Node* head);
void DestroyCanvas(char** canvas);
void WriteXPM(const char* xpmFileName, char** canvas);

#endif
