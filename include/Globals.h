#ifndef _GLOBALS_H_
#define _GLOBALS_H_

struct Node;

const char* FileName;
int TotalRects;
int MaxWidth;
int MaxHeight;
struct Node* Head;

struct Node* MinAreaNode;
struct Node* MaxAreaNode;
struct Node* AreaSortedHead;
struct Node* ColorSortedHead;

struct Node* HeadSorted;
struct Node* MinAreaNodeSorted;
struct Node* MaxAreaNodeSorted;

#endif
