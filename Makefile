CC=gcc
EXECUTABLE=hw5
OBJECT_DIR=object
ICNLUDE_DIR=include
SOURCE_DIR=source

CFLAGS=-g -std=c11 -Werror -Wall -O0 -Wextra -I$(ICNLUDE_DIR)
LIBS=

# Find all source files
SOURCE_NAMES=$(shell cd $(SOURCE_DIR); find -name '*.c' -printf '%P\n' )
# Get all object files by substituting .c with .o
OBJECT_NAMES=$(SOURCE_NAMES:%.c=%.o)
OBJECT_PATHS=$(patsubst %,$(OBJECT_DIR)/%,$(OBJECT_NAMES))

.PHONY: clean

$(OBJECT_DIR)/%.o: $(SOURCE_DIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS)

$(EXECUTABLE): $(OBJECT_PATHS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f $(OBJECT_DIR)/*.o
	rm -f $(EXECUTABLE)
